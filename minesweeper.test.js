
const Cell = require('./mindsweeper');
describe('Cell', () => {
    it('Should return the Cell with the provided properties', () => {
        const result = Cell(1, 2,true,true,true,3);
        expect(result).toEqual({
            id: '12',
            row: 1,
            column: 2,
            opened: true,
            flagged: true,
            mined: true,
            neighbourMineCount: 3
        })
    })
});

const mockMath = Object.create(global.Math);
mockMath.random = () => 0.5;
global.Math = mockMath;

const getRandomInteger = require('./mindsweeper');
describe('getRandomInteger', () => {
    it('Should return 1', () => {
        const result = getRandomInteger(0,2);
        expect(result).toBe(1);
    })
});

const randomlyAssignMines = require('./mindsweeper');
describe('randomlyAssignMines', () => {
    it('Should return the board with mines', () => {
        var testboard = {};
        for (var row = 0; row < 2; row++)
        {
            for(var column = 0; column < 1; column++)
            {
                testboard[row + "" + column] = Cell(row, column, false, false, false, 0);
            }
        }
        const result = randomlyAssignMines(testboard,2,2);
        expect(result).toEqual({
         '00':
            { id: '00',
                row: 0,
                column: 0,
                opened: false,
                flagged: false,
                mined: false,
                neighbourMineCount: 0 },
         '01':
            { id: '01',
                row: 0,
                column: 1,
                opened: false,
                flagged: false,
                mined: false,
                neighbourMineCount: 0 }

        })
    })
});
