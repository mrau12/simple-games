module.exports = Cell;


function Cell(row, column, opened, flagged, mined, neighbourMineCount)
{
    return {
        id: row + "" + column,
        row: row,
        column: column,
        opened: opened,
        flagged: flagged,
        mined: mined,
        neighbourMineCount: neighbourMineCount
    }
}
function Board(boardSize, mineCount)
{
    var board = {};
    for (var row = 0; row < boardSize; row++)
    {
        for(var column = 0; column < boardSize; column++)
        {
           board[row + "" + column] = Cell(row, column, false, false, false, 0);
        }
    }
    board = randomlyAssignMines(board, mineCount);
    board = calculateNeighborMineCounts(board, boardSize);
    return board;
}


var randomlyAssignMines = function(board, mineCount, boardSize)
{
    var mineCoordinates = [];
    for(var i =0; i < mineCount; i++)
    {
        var randomRowCoordinate = getRandomInteger(0, boardSize);
        var randomColumnCoordinate = getRandomInteger(0, boardSize);
        var cell = randomRowCoordinate + "" + randomColumnCoordinate;
        while(mineCoordinates.includes(cell))
        {
            var randomRowCoordinate = getRandomInteger(0, boardSize);
            var randomColumnCoordinate = getRandomInteger(0, boardSize);
            cell = randomRowCoordinate + "" + randomColumnCoordinate;
        }
        mineCoordinates.push(cell);
        board[cell].mined = true;
    }
    return board;
};
module.exports = randomlyAssignMines;

var getRandomInteger = function(min, max)
{
    return Math.floor(Math.random() * (max - min)) + min;
};
module.exports = getRandomInteger;

var calculateNeighborMineCounts = function(board, boardSize)
{
    var cell;
    var neighborMineCount = 0;
    for (var row = 0; row < boardSize; row++)
        for (var column = 0; column < boardSize; column++) {
            var id = row + "" + column;
            cell = board[id];
            if (!cell.mined) {
                var neighbors = getNeighbors(id);
                neighborMineCount = 0;
                for(var i = 0; i < neighbors.length; i++)
                {
                    neighborMineCount += isMined(board, neighbors[i]);
                }
                cell.neighborMineCount = neighborMineCount;
            }
        }
    return board;
};

var getNeighbors = function(id)
{
    // say you make cell1 = Cell(1,1,false,false,false,0)
    // internally cell1 has an: id: row + "" + column, row: row, column: column,
    // so '11'[0] will return the row '1' and '11'[1] will return '1'
    var row = parseInt(id[0]);
    var column = parseInt(id[1]);
    var neighbors = [];
    neighbors.push((row-1) + "" + (column-1));
    neighbors.push((row-1) + "" + column);
    neighbors.push((row-1) + "" + (column+1));
    neighbors.push(row + "" + (column-1));
    neighbors.push(row + "" + (column-1));
    neighbors.push((row+1) + "" + (column-1));
    neighbors.push((row+1) + "" + column);
    neighbors.push((row+1) + "" + (column+1));
    for(var i =0; i <neighbors.length; i++)
    {
        if(neighbors[i].length > 2) // finds border neighbor ids like: '-10', '4-2', '310' that aren't supposed to exist
        {
            neighbors.splice(i,1); // removes (is destructive) elements splice(from,until)
            i--;
        }
    }
    return neighbors
};

var isMined = function(board, row, column)
{
    var cell = board[row + "" + column];
    var mined = 0;
    if (typeof cell !=='undefined')
    {
        mined = cell.mined ? 1: 0;
    }
    return mined;
};

var handleClick = function(id)
{
    if (!gameOver)
    {
        if(crtlIsPressed)
        {
            handleCrtlPressed(id);
        }
        else
        {
            var cell = board[id];
            var $cell = $('#' + id); // prefix $ to variables that contain a jQuery object so more easily identified
            if (!cell.opened)
            {
                if(!cell.flagged)
                {
                    if (cell.mined)
                    {
                        loss();
                        $cell.html(MINE).css('color', 'red');
                    }
                    else
                    {
                        cell.opened = true;
                        if (cell.neightborMineCount > 0)
                        {
                            var color = getNumberColor =(cell.neighborMineCount);
                            $cell.html(cell.neighborMineCount).css('color', color);
                        }
                        else
                        {
                            $cell.html("")
                                .css('background-image', 'radial-gradient(#e6e6e6,#c9c7c7)');
                            var neighbors = getNeighbors(id);
                            for(var i = 0; i < neighbors.length; i++)
                            {
                                var neighbor = neighbors[i];
                                if (typeof board[neighbor] !== 'undefined' && !board[neighbor].flagged && !board[neighbor].opened )
                                {
                                    handleClick(neighbor);
                                }
                            }
                        }
                    }
                }
            }
        }
    }
};

var loss = function()
{
    gameOver = true;
    $('#messageBox').text('Game Over!')
        .css({'color':'white',
        'background-color':'red'});
    var cells = Object.keys(board);
    for(var i = 0; i < cells.length; i++){
        if(board[cells[i]].mined && !board[cells[i]].flagged){
            $('#' + board[cells[i]].id).html(MINE)
                .css('color', 'black');

        }
    }
    clearInterval(timeout);
}

var getNumberColor = function(number)
{
    var color = 'black';
    if(number === 1)
    {
        color = 'blue';
    }
    else if (number === 2)
    {
        color = 'green';
    }
    else if (number === 3)
    {
        color = 'red';
    }
    else if (number === 4)
    {
        color = 'orange';
    }
    return color;
}

var handleRightClick = function(id)
{
    if(!gameOver){
        var cell = board[id];
        var $cell = $('#' + id);
        if(!cell.opened)
        {
            if(!cell.flagged && minesRemaining > 0){
                cell.flagged = true;
                $cell.html(FLAG).css('color','red');
                minesRemaining--;
            }
            else if(cell.flagged){
                cell.flagged = false;
                $cell.html("").css('color','black');
                minesRemaining++;
            }
            $('#mines-remaining').text(minesRemaining);
        }
    }
}


